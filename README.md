[![pipeline status](https://gitlab.com/wizbii-open-source/wallet-management-api/badges/master/pipeline.svg)](https://gitlab.com/wizbii-open-source/wallet-management-api/commits/master)
[![coverage report](https://gitlab.com/wizbii-open-source/wallet-management-api/badges/master/coverage.svg)](https://gitlab.com/wizbii-open-source/wallet-management-api/commits/master)

Objectives
----------

This API is for test purposes only. It aims to implement a (very simple) Wallet Management API where users can create a wallet, add transactions, ... Its main purpose is to allow technical tests for candidates 
It depends on [Mongo Bundle](https://gitlab.com/wizbii-open-source/mongo-bundle), a bundle created at Wizbii to manage interactions with MongoDB. For this particular API, mongodb is fully mocked, allowing to play with this API without any working local mongodb instance.

Prerequisites
------------

-  PHP >= 7.4
-  [composer](https://getcomposer.org/)
-  [symfony](https://symfony.com/download)

Installation
------------

```
git clone git@gitlab.com:wizbii-open-source/wallet-management-api.git
cd wallet-management-api
composer install
```

Run as API
----------

`symfony serve`

Run tests and code validation
-----------------------------

```
# Run unitary tests
composer test
# Run unitary tests with coverage report
composer test:coverage
# Check code is correctly written
composer cs:lint
# Fix lint issues
composer cs:fix
# Statically validate code
composer phpstan
# Run same checks Gitlab-ci will perform
composer dev:checks 
```

Contribute
----------

1. Fork the repository
1. Make your changes
1. Test them with `composer dev:checks`
1. Create a Merge Request
