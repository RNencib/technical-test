<?php

namespace App\Controller;

use App\Model\Wallet\Wallet;
use App\Repository\WalletRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletController extends AbstractController
{
    private WalletRepository $walletRepository;

    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    /**
     * @Route("/wallet/create", methods={"POST"}, name="createWallet")
     */
    public function createWallet(Request $request)
    {
        $newWallet = json_decode((string) $request->getContent(), true);
        $requestedWallet = new Wallet(
            $newWallet['walletId'],
            $newWallet['walletStatus'],
            $newWallet['walletGroupId'],
            (float) $newWallet['walletBalance'],
            (float) $newWallet['walletLowerLimit'] ?? 0.0,
            (float) $newWallet['walletUpperLimit'] ?? 1000.0,
            new \DateTime(),
        );
        $this->walletRepository->create($requestedWallet);

        return new Response($requestedWallet->getId().' was created.', Response::HTTP_CREATED);
    }

    /**
     * @Route("/wallet/{group}/{row}/{offset}", methods={"GET"}, name="getWalletByGroup")
     */
    public function getWalletByGroup($group, $row, $offset)
    {
        $wallets = $this->walletRepository->findWalletsForGroupSortedByDateDescending($group, $row, $offset);
        $walletsCollection = [];

        foreach ($wallets as $wallet) {
            $walletsCollection[] = $wallet->serialize();
        }

        return new JsonResponse($walletsCollection, Response::HTTP_OK);
    }

    /**
     * @Route("/wallet/lock", methods={"POST"}, name="lockWallet")
     */
    public function lockWallet(Request $request)
    {
        $data = json_decode((string) $request->getContent(), true);
        $this->walletRepository->setStatus($data['walletId'], Wallet::STATUS_LOCKED);

        return new Response($data['walletId'].' was '.Wallet::STATUS_LOCKED, Response::HTTP_OK);
    }
}
