<?php

namespace App\Controller;

use App\Model\Transaction\RequestedTransaction;
use App\Operation\TransactionOperation;
use App\Repository\SignedTransactionRepository;
use App\Resources\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Wizbii\MongoBundle\Exception\DocumentDoesNotExistException;

class TransactionController extends AbstractController
{
    private SignedTransactionRepository $signedTransactionRepository;
    private TransactionOperation $transactionOperation;

    public function __construct(SignedTransactionRepository $signedTransactionRepository, TransactionOperation $transactionOperation)
    {
        $this->signedTransactionRepository = $signedTransactionRepository;
        $this->transactionOperation = $transactionOperation;
    }

    /**
     * @Route("/v1/transactions/{walletId}", methods={"GET"})
     */
    public function findTransactionsForAWalletId(string $walletId, Request $request)
    {
        $transactions = Transaction::buildMultiple(
            $this->signedTransactionRepository->findTransactionsSortedByDateDescending($walletId, $request->get('rows', 10), $request->get('offset', 0))
        );

        return new JsonResponse($transactions);
    }

    /**
     * @Route("/v1/transaction/{transactionId}", methods={"GET"}, name="getTransaction")
     */
    public function getTransaction(string $transactionId)
    {
        try {
            $transaction = Transaction::buildOne(
                $this->signedTransactionRepository->get($transactionId)
            );
        } catch (DocumentDoesNotExistException $e) {
            throw new NotFoundHttpException("cannot find transaction with id '$transactionId'");
        }

        return new JsonResponse($transaction);
    }

    /**
     * @Route("/v1/transaction", methods={"POST"})
     */
    public function createTransaction(Request $request)
    {
        $sentTransaction = json_decode((string) $request->getContent(), true);
        $requestedTransaction = new RequestedTransaction(
            $sentTransaction['senderWalletId'] ?? '',
            $sentTransaction['receiverWalletId'] ?? '',
            new \DateTime(),
            (float) $sentTransaction['amount'] ?? 0,
            $sentTransaction['comment'] ?? null,
            $sentTransaction['data'] ?? []
        );
        $signedTransaction = $this->transactionOperation->createTransaction($requestedTransaction);
        $this->signedTransactionRepository->create($signedTransaction);

        return new Response('', Response::HTTP_CREATED, [
            'Location' => $this->generateUrl('getTransaction', ['transactionId' => $signedTransaction->getId()]),
            'X-Transaction-Id' => $signedTransaction->getId(),
        ]);
    }
}
