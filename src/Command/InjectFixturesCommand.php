<?php

namespace App\Command;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\SignedTransaction;
use App\Repository\SignedTransactionRepository;
use App\Signature\SignatureAlgorithm;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InjectFixturesCommand extends Command
{
    protected static $defaultName = 'wallet:fixtures:inject';

    private SignedTransactionRepository $signedTransactionRepository;
    private SignatureAlgorithm $signatureAlgorithm;

    public function __construct(SignedTransactionRepository $signedTransactionRepository, SignatureAlgorithm $signatureAlgorithm)
    {
        parent::__construct();
        $this->signedTransactionRepository = $signedTransactionRepository;
        $this->signatureAlgorithm = $signatureAlgorithm;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->signedTransactionRepository->create($this->buildTransaction('abcd', 'remi', 'mark', 10.3));
        $this->signedTransactionRepository->create($this->buildTransaction('efgh', 'remi', 'john', 30.1));
        $this->signedTransactionRepository->create($this->buildTransaction('ijkl', 'mark', 'remi', 3.21));
        $this->signedTransactionRepository->create($this->buildTransaction('mnop', 'mark', 'remi', 17.13));
        $this->signedTransactionRepository->create($this->buildTransaction('qrst', 'mark', 'john', 3));

        return 0;
    }

    private function buildTransaction(string $transactionId, string $senderWalletId, string $receiverWalletId, float $amount): SignedTransaction
    {
        $requestedTransaction = new RequestedTransaction($senderWalletId, $receiverWalletId, new \DateTime(), $amount, null, []);

        return SignedTransaction::create(
            $transactionId,
            $this->signatureAlgorithm->sign($requestedTransaction),
            $requestedTransaction
        );
    }
}
