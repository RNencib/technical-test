<?php

namespace App\Exception;

class MinimumWalletBalanceExceededException extends \Exception
{
    public function __construct(float $minimumBalance, float $transactionAmount)
    {
        parent::__construct("Minimul wallet balance ($minimumBalance) will be reached with a transaction of $transactionAmount", ExceptionCode::MINIMUM_WALLET_BALANCE_EXCEEDED);
    }
}
