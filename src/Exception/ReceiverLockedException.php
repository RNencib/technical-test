<?php

namespace App\Exception;

class ReceiverLockedException extends \Exception
{
    public function __construct(string $receiverWalletId)
    {
        parent::__construct("$receiverWalletId is locked transaction fail", ExceptionCode::RECEIVER_LOCKED);
    }
}
