<?php

namespace App\Exception;

class WalletIdAlreadyTaken extends \Exception
{
    public function __construct($walletId = '', $code = 0, \Throwable $previous = null)
    {
        parent::__construct("$walletId is already taken. Cannot override", $code, $previous);
    }
}
