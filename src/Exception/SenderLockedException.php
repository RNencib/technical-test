<?php

namespace App\Exception;

class SenderLockedException extends \Exception
{
    public function __construct(string $senderWalletId)
    {
        parent::__construct("$senderWalletId is locked transaction fail", ExceptionCode::SENDER_LOCKED);
    }
}
