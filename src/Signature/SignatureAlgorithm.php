<?php

namespace App\Signature;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;

interface SignatureAlgorithm
{
    public function sign(RequestedTransaction $transaction): Signature;

    public function isValid(SignedTransaction $transaction): bool;

    public function supports(Signature $signature): bool;
}
