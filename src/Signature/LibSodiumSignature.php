<?php

namespace App\Signature;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;

class LibSodiumSignature implements SignatureAlgorithm
{
    private string $secret;
    private const SIGNATURE_NAME = 'libsodium-v1';

    public function __construct(string $secret)
    {
        if (mb_strlen($secret) < SODIUM_CRYPTO_GENERICHASH_BYTES_MIN || mb_strlen($secret) > SODIUM_CRYPTO_GENERICHASH_BYTES_MAX) {
            throw new \Exception('Unsupported secret length : '.mb_strlen($secret).'. Must be between '.SODIUM_CRYPTO_GENERICHASH_BYTES_MIN.' and '.SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);
        }
        $this->secret = $secret;
    }

    public function sign(RequestedTransaction $transaction): Signature
    {
        $hash = $this->buildHash($this->buildMessage($transaction));

        return new Signature(self::SIGNATURE_NAME, $hash);
    }

    public function isValid(SignedTransaction $transaction): bool
    {
        $hash = $this->buildHash($this->buildMessage($transaction));

        return $hash === $transaction->getSignature()->getValue();
    }

    public function supports(Signature $signature): bool
    {
        return self::SIGNATURE_NAME === $signature->getAlgorithm();
    }

    private function buildMessage(RequestedTransaction $transaction): string
    {
        return join('|', [
            $transaction->getSenderWalletId(),
            $transaction->getReceiverWalletId(),
            $transaction->getAmount(),
            $transaction->getCreationDate()->format(\DateTime::ISO8601),
        ]);
    }

    private function buildHash(string $message): string
    {
        return bin2hex(sodium_crypto_generichash($message, $this->secret));
    }
}
