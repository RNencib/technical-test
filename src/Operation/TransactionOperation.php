<?php

namespace App\Operation;

use App\Exception\ReceiverLockedException;
use App\Exception\SenderLockedException;
use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\SignedTransaction;
use App\Repository\WalletRepository;
use App\Signature\SignatureAlgorithm;
use Ramsey\Uuid\Uuid;

class TransactionOperation
{
    private WalletRepository $walletRepository;
    private SignatureAlgorithm $signatureAlgorithm;

    public function __construct(WalletRepository $walletRepository, SignatureAlgorithm $signatureAlgorithm)
    {
        $this->walletRepository = $walletRepository;
        $this->signatureAlgorithm = $signatureAlgorithm;
    }

    public function createTransaction(RequestedTransaction $requestedTransaction): SignedTransaction
    {
        $senderWallet = $this->walletRepository->get($requestedTransaction->getSenderWalletId());
        $receiverWallet = $this->walletRepository->get($requestedTransaction->getReceiverWalletId());
        if ('locked' == $receiverWallet->getStatus()) {
            throw new ReceiverLockedException($receiverWallet->getId());
        }
        if ('locked' == $senderWallet->getStatus()) {
            throw new SenderLockedException($senderWallet->getId());
        }
        $senderWallet->assertIncomingTransactionIsValid($requestedTransaction);
        $receiverWallet->assertOutgoingTransactionIsValid($requestedTransaction);

        $signedTransaction = SignedTransaction::create(Uuid::uuid4()->toString(), $this->signatureAlgorithm->sign($requestedTransaction), $requestedTransaction);

        return $signedTransaction;
    }
}
