<?php

namespace App\Model\User;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class User implements ArraySerializable
{
    private string $id;
    private string $firstName;
    private string $lastName;

    public function __construct(string $id, string $firstName, string $lastName)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function serialize(array $groups = []): array
    {
        return [
            '_id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return new self($contentAsArray['_id'], $contentAsArray['firstName'], $contentAsArray['lastName']);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
