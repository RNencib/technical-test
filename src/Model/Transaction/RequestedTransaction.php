<?php

namespace App\Model\Transaction;

class RequestedTransaction
{
    protected string $senderWalletId;
    protected string $receiverWalletId;
    protected \DateTime $creationDate;
    protected float $amount;
    protected ?string $comment;
    protected array $data;

    public function __construct(string $senderWalletId, string $receiverWalletId, \DateTime $creationDate, float $amount, ?string $comment, array $data)
    {
        $this->senderWalletId = $senderWalletId;
        $this->receiverWalletId = $receiverWalletId;
        $this->creationDate = $creationDate;
        $this->amount = $amount;
        $this->comment = $comment;
        $this->data = $data;
    }

    public function getSenderWalletId(): string
    {
        return $this->senderWalletId;
    }

    public function getReceiverWalletId(): string
    {
        return $this->receiverWalletId;
    }

    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
