<?php

namespace App\Tests\Signature;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;
use App\Signature\LibSodiumSignature;
use PHPUnit\Framework\TestCase;

class LibSodiumSignatureTest extends TestCase
{
    public function test_it_can_generate_signature()
    {
        $algorithm = new LibSodiumSignature('my_secret_containing_between_16_and_64_characters');
        $signature = $algorithm->sign($this->getRequestedTransaction());
        $this->assertThat($signature->getAlgorithm(), $this->equalTo('libsodium-v1'));
        $this->assertThat($signature->getValue(), $this->equalTo('0a01e21b8b6005b1da09528c5321eb54d3599a871a905625140177756a5a8f8c'));
    }

    public function test_it_can_validate_valid_signature()
    {
        $algorithm = new LibSodiumSignature('my_secret_containing_between_16_and_64_characters');
        $signedTransaction = SignedTransaction::create(
            'abcd',
            new Signature('libsodium-v1', '0a01e21b8b6005b1da09528c5321eb54d3599a871a905625140177756a5a8f8c'),
            $this->getRequestedTransaction()
        );
        $this->assertThat($algorithm->isValid($signedTransaction), $this->isTrue());
    }

    public function test_it_can_invalidate_invalid_signature()
    {
        $algorithm = new LibSodiumSignature('my_secret_containing_between_16_and_64_characters');
        $signedTransaction = SignedTransaction::create(
            'abcd',
            new Signature('libsodium-v1', '6d868e33b0ffdfdf6aa43b5c52a1642ddb861c69305b6a9a7c6c8fcde49ba1f6'),
            $this->getRequestedTransaction()
        );
        $this->assertThat($algorithm->isValid($signedTransaction), $this->isFalse());
    }

    private function getRequestedTransaction(): RequestedTransaction
    {
        return new RequestedTransaction(
            'wallet-abcd',
            'wallet-efgh',
            new \DateTime('2019-11-30T09:41:33+0100'),
            10.0,
            null,
            []
        );
    }
}
