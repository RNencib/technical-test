<?php

namespace App\Tests\Signature;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;
use App\Signature\StaticSignature;
use PHPUnit\Framework\TestCase;

class StaticSignatureTest extends TestCase
{
    public function test_it_can_generate_signature()
    {
        $algorithm = new StaticSignature('my static value');
        $signature = $algorithm->sign($this->getRequestedTransaction());
        $this->assertThat($signature->getAlgorithm(), $this->equalTo('static'));
        $this->assertThat($signature->getValue(), $this->equalTo('my static value'));
    }

    public function test_it_can_validate_valid_signature()
    {
        $algorithm = new StaticSignature('my static value');
        $signedTransaction = SignedTransaction::create(
            'abcd',
            new Signature('static', 'my static value'),
            $this->getRequestedTransaction()
        );
        $this->assertThat($algorithm->isValid($signedTransaction), $this->isTrue());
    }

    public function test_it_can_invalidate_invalid_signature()
    {
        $algorithm = new StaticSignature('my static value');
        $signedTransaction = SignedTransaction::create(
            'abcd',
            new Signature('static', 'any other value'),
            $this->getRequestedTransaction()
        );
        $this->assertThat($algorithm->isValid($signedTransaction), $this->isFalse());
    }

    private function getRequestedTransaction(): RequestedTransaction
    {
        return new RequestedTransaction(
            'wallet-abcd',
            'wallet-efgh',
            new \DateTime('2019-11-30T09:41:33+0100'),
            10.0,
            null,
            []
        );
    }
}
