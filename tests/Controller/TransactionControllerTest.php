<?php

namespace App\Tests\Controller;

use App\Controller\TransactionController;
use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;
use App\Repository\SignedTransactionRepository;
use App\Signature\StaticSignature;
use PHPUnit\Framework\Constraint\Constraint;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TransactionControllerTest extends KernelTestCase
{
    private TransactionController $transactionController;
    private SignedTransactionRepository $signedTransactionRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bootKernel();
        $this->signedTransactionRepository = self::$kernel->getContainer()->get(SignedTransactionRepository::class);
        $signatureAlgorithm = new StaticSignature('my value');
        $this->transactionController = new TransactionController($this->signedTransactionRepository, $signatureAlgorithm);
        $this->transactionController->setContainer(self::$container);
    }

    public function test_it_can_create_transaction()
    {
        $content = '{"senderWalletId":"wallet-a","receiverWalletId":"wallet-b","amount":10.0,"comment":"my payment","data":[]}';
        $request = Request::create('http://localhost:8000/v1/transaction', 'POST', [], [], [], [], $content);
        $response = $this->transactionController->createTransaction($request);
        $this->assertThat($response, $this->isInstanceOf(Response::class));
        $this->assertThat($response->getStatusCode(), $this->equalTo(201));
        $this->assertThat($response->headers->get('X-Transaction-Id'), $this->isNotNull());
        $signedTransaction = $this->signedTransactionRepository->get($response->headers->get('X-Transaction-Id'));
        $this->assertThat($signedTransaction, $this->isInstanceOf(SignedTransaction::class));
        $this->assertThat($signedTransaction->getSenderWalletId(), $this->equalTo('wallet-a'));
        $this->assertThat($signedTransaction->getReceiverWalletId(), $this->equalTo('wallet-b'));
        $this->assertThat($signedTransaction->getAmount(), $this->equalTo(10.0));
    }

    public function test_it_can_get_existing_transaction()
    {
        $this->signedTransactionRepository->create($this->getSignedTransaction('abcd'));
        $response = $this->transactionController->getTransaction('abcd');
        $this->assertThat($response, $this->isInstanceOf(Response::class));
        $this->assertThat($response->getStatusCode(), $this->equalTo(200));
        $content = json_decode($response->getContent(), true);
        $this->assertThat($content['senderWalletId'], $this->equalTo('wallet-a'));
        $this->assertThat($content['receiverWalletId'], $this->equalTo('wallet-b'));
        $this->assertThat($content['amount'], $this->equalTo(10.0));
    }

    public function test_it_returns_404_on_non_existing_transaction()
    {
        $this->expectException(NotFoundHttpException::class);
        $this->transactionController->getTransaction('abcd');
    }

    private function getSignedTransaction(string $transactionId): SignedTransaction
    {
        return SignedTransaction::create(
            $transactionId,
            new Signature('static', 'my value'),
            new RequestedTransaction(
                'wallet-a',
                'wallet-b',
                new \DateTime('2019-11-30T09:41:33+0100'),
                10.0,
                null,
                []
            )
        );
    }

    private function isNotNull(): Constraint
    {
        return $this->logicalNot($this->isNull());
    }
}
