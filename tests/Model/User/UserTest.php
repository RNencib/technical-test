<?php

namespace App\Tests\Model\User;

use App\Model\User\User;
use Tests\Wizbii\JsonSerializerBundle\ModelTestCase;

class UserTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return User::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/user';
    }
}
