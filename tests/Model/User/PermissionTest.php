<?php

namespace App\Tests\Model\User;

use App\Model\User\Permission;
use Tests\Wizbii\JsonSerializerBundle\ModelTestCase;

class PermissionTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return Permission::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/permission';
    }

    public function test_it_can_validate_read_wallet_permission()
    {
        $permission = new Permission('remi-alvado', [Permission::TYPE_READ_WALLET]);
        $this->assertThat($permission->canReadWallet('remi-alvado'), $this->isTrue());

        $permission = new Permission('remi-alvado', [Permission::TYPE_CREATE_TRANSACTION]);
        $this->assertThat($permission->canReadWallet('remi-alvado'), $this->isFalse());
    }

    public function test_it_can_validate_create_transaction_permission()
    {
        $permission = new Permission('remi-alvado', [Permission::TYPE_CREATE_TRANSACTION]);
        $this->assertThat($permission->canCreateTransaction('remi-alvado'), $this->isTrue());

        $permission = new Permission('remi-alvado', [Permission::TYPE_READ_WALLET]);
        $this->assertThat($permission->canCreateTransaction('remi-alvado'), $this->isFalse());
    }
}
