<?php

namespace App\Tests\Model\Transaction;

use App\Model\Transaction\SignedTransaction;
use Tests\Wizbii\JsonSerializerBundle\ModelTestCase;

class SignedTransactionTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return SignedTransaction::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/signed_transaction';
    }
}
