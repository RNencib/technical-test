<?php

return App\Model\Transaction\SignedTransaction::create(
    'abc',
    new \App\Model\Transaction\Signature('static', 'abcd'),
    new \App\Model\Transaction\RequestedTransaction(
        'wallet-wizbii-stock',
        'wallet-remi-alvado-giveaway',
        new \DateTime('2019-11-30T09:41:33+0100'),
        2.0,
        null,
        []
    )
);
