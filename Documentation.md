Documentation
-----------

Create Wallet (POST): /wallet/create
------------------------------------
Content: 
```json
{
  "walletId":"wallet-id",
  "walletStatus":"active",
  "walletGroupId":"group-id",
  "walletBalance":60,
  "walletLowerLimit":0,
  "walletUpperLimit":100
}
```

Get Wallets Group (GET): /wallet/{group}/{row}/{offset}
-------------------------------------------------------
group = groupId

row = the number of wallets to show

offset = the start of wallets to show

Lock A Wallet (POST): /wallet/lock
----------------------------------

Content:
```json
{
  "walletId":"wallet-id"
}
```